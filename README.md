# Art Crawler
Art crawler is an application to view and favorite artworks crawled from various sources

The crawled data is currently being stored in a SQLite database

The currently added crawlers are
* [The Athenaeum](http://www.the-athenaeum.org/)
* [Bukowskis](https://www.bukowskis.com/en)
* [Van Gogh Museum](https://www.vangoghmuseum.nl)


# Application images
## Index.html
![Index.html](docs/artwork.JPG)

## List view
![List view](docs/listview.JPG)