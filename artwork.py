import os
import io
import urllib
from pathlib import Path
from PIL import Image

import requests
from slugify import slugify


SIZE = 1024, 1024

class Artwork():
    def __init__(self, title, artist, year, image_url):
        if None in [title, artist, image_url]:
            raise ValueError('Something is missing')

        self.title = title
        self.url_title = slugify(title)
        self.artist = artist
        self.url_artist = slugify(artist)
        self.year = year
        self.image = self._download_and_resize_image(image_url)

    def __repr__(self):
        return '{} - {}'.format(self.url_title, self.url_artist)
        
    def _download_and_resize_image(self, image_url):
        response = requests.get(image_url)

        if response.status_code != 200:
            raise requests.HTTPError("Failed to get image. Error code {}".format(response.status_code))

        image = Image.open(io.BytesIO(response.content))
        image.thumbnail(SIZE, Image.LANCZOS)

        imgByteArr = io.BytesIO()
        image.save(imgByteArr, format="JPEG")
        return imgByteArr.getvalue()