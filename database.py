import sys
import sqlite3
from datetime import datetime

# SQLite does not support UPSERT in earlier versions
MIN_PYTHON = (3,7,4)
if sys.version_info < MIN_PYTHON:
    sys.exit("Python %s.%s.%s or later is required.\n" % MIN_PYTHON)


class Database():
    def __enter__(self):
        self.connection = sqlite3.connect('artform.db')
        self.cursor = self.connection.cursor()

        self.cursor.execute('''
            CREATE TABLE IF NOT EXISTS artists (
                name        TEXT    NOT NULL UNIQUE,
                url_name    TEXT    NOT NULL UNIQUE
            )
        ''')

        self.cursor.execute('''
            CREATE TABLE IF NOT EXISTS favorites (
                artwork_id INT NOT NULL UNIQUE,
                favorite INT NOT NULL,
                FOREIGN KEY (artwork_id) REFERENCES artwork (ROWID)
            )
        ''')

        self.cursor.execute('''
            CREATE TABLE IF NOT EXISTS artworks (
                title       TEXT    NOT NULL,
                artist_id   INT     NOT NULL,
                year        TEXT    NULL,
                image       BLOB    NOT NULL UNIQUE,
                FOREIGN KEY (artist_id) REFERENCES artists (ROWID)
            )
        ''')

        self.connection.commit()

        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.connection.commit()
        self.connection.close()

    def _get_artist_id(self, artist, url_artist):
        self.cursor.execute('''
            SELECT 
                ROWID 
            FROM 
                artists
            WHERE 
                url_name = ?
        ''', (url_artist,))

        result = self.cursor.fetchone()

        if result is None:
            self.cursor.execute('''
                INSERT INTO 
                    artists (name, url_name)
                VALUES 
                    (?, ?)
            ''', (artist, url_artist,))

            return self.cursor.lastrowid
        else:
            return result[0]

    def add_artworks(self, artworks):
        print('\n######## ADDING ENTRIES TO DB: {} ########\n'.format(datetime.now().strftime("%Y-%m-%d %H:%M:%S")))
        [self.add_artwork(entry) for entry in artworks]

    def add_artwork(self, artwork):
        artist_id = self._get_artist_id(artwork.artist, artwork.url_artist)

        data = (
            artwork.title,
            artwork.year,
            artist_id,
            artwork.image
        )
        try:
            self.cursor.execute('''
                INSERT INTO
                    artworks (title, year, artist_id, image)
                VALUES
                    (?, ?, ?, ?)
            ''', data)
        except sqlite3.IntegrityError:
            print('{} already exists in DB!'.format(artwork.title))
            return

        self.connection.commit()

    def get_random_entry(self):
        self.cursor.execute('''
            SELECT
                AW.ROWID,
                AW.title
            FROM 
                artworks AW
            WHERE
                AW.ROWID >= (abs(random()) % (SELECT max(ROWID) from artworks)) + 1
            LIMIT 1
        ''')

        id, title = self.cursor.fetchone()

        return id, title

    def get_random_entry_id(self):
        self.cursor.execute('''
            SELECT
                AW.ROWID
            FROM 
                artworks AW
            WHERE
                AW.ROWID >= (abs(random()) % (SELECT max(ROWID) from artworks)) + 1
            LIMIT 1
        ''')

        id = self.cursor.fetchone()[0]

        return id

    def get_works_by_artist(self, artist_url_name, page):
        self.cursor.execute('''
            SELECT
                AW.ROWID,
                AW.title,
                AW.image
            FROM
                artworks AW
            INNER JOIN 
                artists A
                    ON A.ROWID = AW.artist_id
                        AND A.url_name = ?
            ORDER BY AW.title
            LIMIT 25
            OFFSET ?
        ''', (artist_url_name, 25 * (page - 1),))

        artworks = self.cursor.fetchall()

        self.cursor.execute('''
            SELECT
                COUNT(*)
            FROM
                artworks AW
            INNER JOIN 
                artists A
            ON
                A.ROWID = AW.artist_id
                AND A.url_name = ?
        ''', (artist_url_name,))

        total_amount = self.cursor.fetchone()[0]

        # Get artist name
        self.cursor.execute('''
            SELECT
                name
            FROM
                artists
            WHERE
                url_name = ?
        ''', (artist_url_name,))

        try:
            artist_name = self.cursor.fetchone()[0]
        except TypeError:
            print('Failed to find artist')
            return

        return (
            artworks,
            artist_name,
            total_amount
        )

    def get_artwork(self, id):
        self.cursor.execute('''
            SELECT
                AW.ROWID,
                AW.title,
                CASE WHEN AW.Year IS NULL THEN 'Unknown year' ELSE AW.year END,
                A.name,
                A.url_name,
                AW.image,
                CASE WHEN F.artwork_id is NULL THEN 0 ELSE F.favorite END AS favorited
            FROM 
                artworks AW
            INNER JOIN artists A
                ON A.ROWID = AW.artist_id
            LEFT JOIN favorites F
                ON AW.ROWID = F.artwork_id
            WHERE AW.ROWID = ?
        ''', (id,))

        return self.cursor.fetchone()

    def favorite_artwork(self, id):
        self.cursor.execute('''
            INSERT INTO 
                favorites
            SELECT
                ROWID, 1
            FROM 
                artworks
            WHERE
                ROWID = :artwork_id
            ON CONFLICT(artwork_id)
            DO UPDATE 
                SET favorite = 
                    CASE (SELECT favorite FROM favorites WHERE artwork_id = :artwork_id)
                        WHEN 1 THEN 0
                        WHEN 0 THEN 1
                    END
        ''', {'artwork_id': id})

        self.cursor.execute('''
            SELECT
                favorite
            FROM
                favorites
            WHERE
                artwork_id = ?
        ''', (id,))

        return self.cursor.fetchone()[0]

    def get_favorited_artworks(self, page):
        self.cursor.execute('''
            SELECT
                AW.ROWID as id,
                AW.image
            FROM 
                artworks AW
            INNER JOIN 
                favorites F
                ON F.artwork_id = AW.ROWID
                AND F.favorite = 1
            ORDER BY AW.ROWID
            LIMIT 25
            OFFSET ?
        ''', (25 * (page - 1),))

        favorited_artworks = self.cursor.fetchall()

        self.cursor.execute('''
            SELECT
                COUNT(*)
            FROM 
                artworks AW
            INNER JOIN 
                favorites F
                ON F.artwork_id = AW.ROWID
                AND F.favorite = 1
            INNER JOIN 
                artists A
                ON AW.artist_id = A.ROWID
        ''')

        total_amount = self.cursor.fetchone()[0]

        return favorited_artworks, total_amount

    def search_artworks(self, search, page):
        self.cursor.execute('''
            SELECT
                AW.ROWID as id,
                AW.image
            FROM
                artworks AW
            INNER JOIN
                artists A
                    ON A.ROWID = AW.artist_id
            WHERE
                (
                    LOWER(AW.title) LIKE LOWER(:search) || ' %'
                    OR LOWER(AW.title) LIKE ' %' || LOWER(:search) || ' %'
                    OR LOWER(AW.title) LIKE LOWER(:search) || 's %'
                    OR LOWER(AW.title) = LOWER(:search)
                )
                OR
                (
                    LOWER(A.name) LIKE LOWER(:search)
                )
            ORDER BY AW.title
            LIMIT 25
            OFFSET :page
        ''', {'search': search, 'page': 25 * (page - 1)})

        search_result = self.cursor.fetchall()

        self.cursor.execute('''
            SELECT
                COUNT(*)
            FROM
                artworks AW
            INNER JOIN 
                artists A
            ON
                A.ROWID = AW.artist_id
            WHERE
                (
                    LOWER(AW.title) LIKE LOWER(:search) || ' %'
                    OR LOWER(AW.title) LIKE ' %' || LOWER(:search) || ' %'
                    OR LOWER(AW.title) LIKE LOWER(:search) || 's %'
                    OR LOWER(AW.title) = LOWER(:search)
                )
                OR
                (
                    LOWER(A.name) LIKE LOWER(:search)
                )
        ''', {'search': search})

        total_amount = self.cursor.fetchone()[0]

        return search_result, total_amount

    def get_artworks_from_year(self, from_year, to_year, page):
        arguments = {
            'from': from_year,
            'page': 25 * (page - 1),
            'to': to_year
        }

        self.cursor.execute('''
            SELECT
                AW.ROWID as id,
                AW.image
            FROM
                artworks AW
            INNER JOIN 
                artists A
            ON
                A.ROWID = AW.artist_id
            WHERE
                CASE WHEN :to IS NOT NULL THEN
                    AW.year BETWEEN :from AND :to
                ELSE
                    AW.year = :from
                END
            ORDER BY AW.year
            LIMIT 25
            OFFSET :page
        ''', arguments)

        search_result = self.cursor.fetchall()

        self.cursor.execute('''
            SELECT
                COUNT(*)
            FROM
                artworks AW
            INNER JOIN 
                artists A
            ON
                A.ROWID = AW.artist_id
            WHERE
                CASE WHEN :to IS NOT NULL THEN
                    AW.year BETWEEN :from AND :to
                ELSE
                    AW.year = :from
                END
        ''', arguments)

        total_amount = self.cursor.fetchone()[0]

        return search_result, total_amount
