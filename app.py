import base64
import json
import os
import sys
from pathlib import Path

import requests
from flask import Flask, Response, redirect, render_template, url_for
from slugify import slugify

from artwork import Artwork
from crawlers import athenaeum, bukowskis, vangoghmuseum
from database import Database


def main():
    athenaeum.main()
    #bukowskis.main()
    pass

if __name__ == '__main__':
    main()

app = Flask(__name__)


@app.route('/artwork/')
@app.route('/')
def index():
    with Database() as db:
        id, title = db.get_random_entry()
        return redirect('/artwork/{}/{}'.format(id, slugify(title)))


@app.route('/artwork/<int:id>/')
@app.route('/artwork/<int:id>/<url_title>/')
def get_artwork_with_id(id, url_title=None):
    with Database() as db:
        (
            id,
            title,
            year,
            name,
            url_name,
            image,
            favorited
        ) = db.get_artwork(id)

        slugified_title = slugify(title)

        if url_title != slugified_title:
            return redirect('/artwork/{}/{}/'.format(id, slugified_title))

        return render_template(
            'index.html',
            id=id,
            title=title,
            year=year,
            artist_name=name,
            artist_url=url_name,
            image=base64.encodebytes(image).decode('utf-8'),
            favorited=favorited
        )


@app.route('/search/<search_text>/')
@app.route('/search/<search_text>/page/<int:page>/')
def search(search_text, page=1):
    with Database() as db:
        result, total_amount = db.search_artworks(search_text, page)

        if result is None:
            return redirect('/')

        artworks = [{
            'id': entry[0],
            'image': base64.encodebytes(entry[1]).decode('utf-8')
        } for entry in result]

        return render_template(
            'search_results.html',
            artworks=artworks,
            title='Search result for {}'.format(search_text),
            total_amount=total_amount,
            base_url='/search/{}'.format(search_text),
            current_page=page
        )


@app.route('/artist/<artist_url_name>/')
@app.route('/artist/<artist_url_name>/page/')
@app.route('/artist/<artist_url_name>/page/<int:page>/')
def get_works_by_artist(artist_url_name, page=None):
    if page is None:
        return redirect('/artist/{}/page/1/'.format(artist_url_name))

    with Database() as db:
        artworks, artist_name, total_amount = db.get_works_by_artist(artist_url_name, page)

        if len(artworks) == 0:
            return redirect('/')

        artworks = [{
            'id': entry[0],
            'title': slugify(entry[1]),
            'image': base64.encodebytes(entry[2]).decode('utf-8'),
        } for entry in artworks]

        return render_template(
            'artist.html',
            artworks=artworks,
            artist_name=artist_name,
            total_amount=total_amount,
            current_page=page,
            base_url="/artist/{}".format(artist_url_name)
        )


@app.route('/favorites/')
@app.route('/favorites/page/')
@app.route('/favorites/page/<int:page>/')
def get_favorited_artworks(page=None):
    if page is None:
        return redirect('/favorites/page/1/')

    with Database() as db:
        artworks = []

        result, total_amount = db.get_favorited_artworks(page)

        for entry in result:
            artworks.append({
                'id': entry[0],
                'image': base64.encodebytes(entry[1]).decode('utf-8')
            })

        return render_template(
            'favorites.html',
            artworks=artworks,
            current_page=page,
            total_amount=total_amount
        )


@app.route('/api/favorite/<int:id>', methods=['POST'])
def favorite_artwork(id):
    with Database() as db:
        result = db.favorite_artwork(id)
        return Response(
            response=json.dumps({"favorite": result}),
            status=200,
            mimetype='application/json'
        )

@app.route('/year/from/<int:from_year>/')
@app.route('/year/from/<int:from_year>/page/<int:page>/')
@app.route('/year/from/<int:from_year>/to/<int:to_year>/')
@app.route('/year/from/<int:from_year>/to/<int:to_year>/page/<int:page>/')
def get_artworks_from_year(from_year, to_year=None, page=1):
    with Database() as db:
        result, total_amount = db.get_artworks_from_year(
            from_year, to_year, page)

        if result is None:
            return redirect('/')

        artworks = [{
            'id': entry[0],
            'image': base64.encodebytes(entry[1]).decode('utf-8')
        } for entry in result]

        title = 'Artworks from year {}'.format(from_year)

        if to_year is not None:
            title = '{} to year {}'.format(title, to_year)

        url = '/year/from/{}'.format(from_year)
        if to_year is not None:
            url = '{}/to/{}'.format(url, to_year)

        return render_template(
            'search_results.html',
            artworks=artworks,
            title=title,
            total_amount=total_amount,
            base_url=url,
            current_page=page
        )
