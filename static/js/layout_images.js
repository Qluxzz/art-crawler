document.addEventListener('DOMContentLoaded', () => {
    const elem = document.querySelector('.grid')

    const masonry = new Masonry(elem, {
        columnWidth: '.grid-sizer',
        gutter: '.gutter-sizer',
        itemSelector: '.artwork'
    })

    let loadedImagesInQueue = 0
    let totalLoaded = 0
    const totalAmountOfImages = document.querySelectorAll("div.artwork").length

    const imageLoaded = () => {
        ++loadedImagesInQueue
        ++totalLoaded

        if (loadedImagesInQueue >= 25 || totalLoaded === totalAmountOfImages) {
            masonry.layout()
            loadedImagesInQueue = 0
        }
    }

    document
        .querySelectorAll("div.artwork > a > img")
        .forEach(x =>
            x.addEventListener("load", imageLoaded)
        )
})