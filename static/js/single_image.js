'use strict'

const favorite = async artworkId => {
  const response = await fetch(
    `/api/favorite/${artworkId}`, {
      method: 'POST'
    }
  )

  if (response.ok) {
    const { favorite } = await response.json()
    setFavoriteStatus(favorite)
  }
}

const setFavoriteStatus = status => {
  const elem = document.querySelector('.favorite-icon')

  if (status)
    elem.classList.add("favorited")
  else
    elem.classList.remove("favorited")
}

const setShowInfo = value => {
  window.localStorage.setItem("showInfo", value)
  const infoSectionClassList = document.querySelector("section").classList

  if (value)
    infoSectionClassList.add("show")
  else
    infoSectionClassList.remove("show")
}

const getShowInfo = () => window.localStorage.getItem("showInfo") === "true"

const toggleShowInfo = () => {
  setShowInfo(!getShowInfo())
}

document.addEventListener('DOMContentLoaded', () => {
  const dataElem = document.getElementById('data')
  const id = parseInt(dataElem.getAttribute('artwork_id'))

  setShowInfo(getShowInfo())

  const favoriteElem = document.querySelector('.favorite-icon')

  favoriteElem.addEventListener('click', async () => favorite(id))

  document.addEventListener('keydown', async event => {
    if (event.target.type === "text")
      return

    switch (event.key) {
      case 'a': // Go to artist
        window.location.href = document.getElementById('artist_name').href
        break
      case 'f': // Favorite artwork
        favorite(id)
        break
      case 'r': // Get new random artwork
        window.location.href = '/'
        break
      case 'i': // Toggle info
        toggleShowInfo()
        break
    }
  })
})
