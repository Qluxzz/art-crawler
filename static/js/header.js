document.addEventListener("submit", event => {
    event.preventDefault()
    event.stopPropagation()
    window.location.href = `/search/${encodeURIComponent(document.getElementById("search_input").value)}`
})