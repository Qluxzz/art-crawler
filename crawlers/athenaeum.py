import requests
from bs4 import BeautifulSoup

from artwork import Artwork
from database import Database

base_path = 'http://www.the-athenaeum.org/art/full.php?ID={}'

def get_page(id):
    response = requests.get(base_path.format(id))

    if response.status_code != 200:
        print(response.status_code)
        return

    if '/art/full.php?ID={}'.format(id) not in response.url:
        print('id: {} was not an image!'.format(id))
        return

    return response.content


def parse_page(page):
    soup = BeautifulSoup(page, features='lxml')

    image_url = soup.find('img', attrs={'id': 'fullimg'})['src']

    info_box = soup.find('div', attrs={'class': 'left'})

    title = info_box.h1.string

    artist = info_box.find('a').string

    info_string = info_box.div.text

    year_start = info_string.find('\n - ')
    year_end = info_string.find('\n', year_start + 1)

    year = info_string[year_start + 4:year_end]

    return Artwork(
        title,
        artist,
        year,
        'http://www.the-athenaeum.org/art/{}'.format(image_url)
    )


def main():
    entries = []

    for id in range(301423, 1500000):
        page = get_page(id)

        if page is None:
            continue

        try:
            entry = parse_page(page)
        except:
            continue

        if entry is None:
            continue

        entries.append(entry)

        if len(entries) == 25:
            with Database() as db:
                db.add_artworks(entries)
            entries.clear()
    
if __name__ == '__main__':
    main()