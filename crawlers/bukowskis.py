import requests
from bs4 import BeautifulSoup

from artwork import Artwork
from database import Database

base_path = 'https://www.bukowskis.com/sv/lots/category/konst/city/all/view/list/page/{}/archive/only?utf8=%E2%9C%93'

# Login sets cookie, get it
cookies = dict(
    _fineart_session='SGxacDVTQlI3S2VKRzhLVkdvTVRETEV2Y3ZhcU9CZnVOWGE0ck5TZjJJV2I3NDlxQVBPZGZucHhvd2FUaExCendQeFVpMHVldFpqRTFtSW1yMCt0L29XdU9OUElRZDdYMXUycTViUjcwa1ZMQ2Z0Z29UWDFNYVRMUXhmUFFZM3pnVUJjZFl6d3RIVEZmK3RkVTk1MU9lMnJ3WTlZVzBtWXJoRHk5Z0hpRkZPN2lTUG9JczVqWjFOZW15by9JV1VGd3hIeXE1amcxT0xHd2loT1JXK0dLVDJIRmEwTENmMnZuWmZ6b3RLbWlYbW43M1F6VmplN3dUd0RIN2VlcUJJZ0t4UUh2S3JTc25KRmFyZU5XY0N2d095VmppNGwxZERMT1pSa3NGUm5qNFkvUnMxM0I2bzNhTEpLc1BYaUNrd2lUU2tNaldLcmxtTDhLSDhEdzE5YUVnPT0tLThvcTlidURyZ1RKQUhoVmgvMXNiWWc9PQ%3D%3D--479d8614db01097ebd7b12e29ffb83973cef938d'
)

def get_page(page_number):
    response = requests.get(
        base_path.format(page_number),
        cookies=cookies
    )

    if response.status_code != 200:
        print(response.status_code)
        return

    return response.content

def parse_page(page):
    soup = BeautifulSoup(page, features='lxml')

    for entry in soup.find_all('div', attrs={'class': 'list-view-item catalogue-item'}):
            parsed_entry = parse_entry(entry)
            if parsed_entry is not None:
                yield parsed_entry

def parse_entry(entry):
    image_url = entry.img['src']

    if image_url is None:
        return

    image_url = image_url.replace('object', 'fullsize')

    info = entry.find('div', attrs={'class': 'new-lot-info'})

    try:
       values = info.h2.string.split(',')
       artist = values[0]
    except ValueError:
        print('Failed to unpack ({})'.format(info.h2.string))
        return
    
    try:
        title = info.p.string.split('.')[0]
    except AttributeError:
        return

    return Artwork(
        title,
        artist,
        None,
        image_url
    )

def main():
    current_page = 10
    entries = []

    while current_page < 100:
        page = get_page(current_page)
        if page is None:
            return

        generator = parse_page(page)

        while True:
            try:
                entry = next(generator)
            except StopIteration:
                print('Done! Page: {}'.format(current_page))
                break

            entries.append(entry)

            if len(entries) == 25:
                with Database() as db:
                    db.add_artworks(entries)
                entries.clear()

        current_page = current_page + 1

if __name__ == '__main__':
    main()