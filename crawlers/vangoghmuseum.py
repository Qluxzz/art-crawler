import requests
from bs4 import BeautifulSoup

from artwork import Artwork
from database import Database

base_path = 'https://www.vangoghmuseum.nl/en/search/collection?q=&pagesize=4000'


def get_document():
    response = requests.get(base_path)

    if response.status_code != 200:
        print(response.status_code)
        return

    return response.content


def parse_document(document):
    soup = BeautifulSoup(document, features='lxml')
    for entry in soup.find_all('li', attrs={'class': 'col'}):
        parsed_entry = parse_entry(entry)
        if parsed_entry is not None:
            yield parsed_entry


def parse_entry(entry):
    image_url = entry.img.get('data-lazy-image-url')

    if image_url is None:
        return

    try:
        values = entry.h3.string.strip().split('\n')
        title = values[0].strip().replace(',', '')
        year = values[1].strip()
    except IndexError:
        print('Failed to find title and year in ({})'.format(
            entry.h3.string.strip()))
        return

    url = 'https:{}=s1200'.format(image_url)
    artist = entry.p.string

    if artist is None:
        return

    artwork = Artwork(
        title,
        artist,
        year,
        url
    )

    return artwork


def main():
    document = get_document()

    if not document:
        return

    generator = parse_document(document)

    entries = []

    while True:
        try:
            entry = next(generator)
        except StopIteration:
            print("Done!")
            return

        entries.append(entry)

        if len(entries) == 25:
            with Database() as db:
                db.add_artworks(entries)
            entries.clear()


if __name__ == '__main__':
    main()
